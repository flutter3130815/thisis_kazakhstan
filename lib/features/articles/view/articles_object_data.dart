import 'package:thisis_kazakhstan/features/user_data.dart';


class ArticlesObjectData {
  String? image;
  String? title;
  String? description;
  UserData? user;

  ArticlesObjectData({
    required this.image,
    required this.title,
    required this.description,
    required this.user,
  });
}
