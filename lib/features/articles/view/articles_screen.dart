import "package:flutter/material.dart";
import "package:thisis_kazakhstan/features/articles/view/articles_object_data.dart";
import "package:thisis_kazakhstan/features/articles/view/articles_route_data.dart";
import "package:thisis_kazakhstan/features/articles/widgets/articles_body.dart";
import "package:thisis_kazakhstan/features/user_data.dart";


class ArticlesScreen extends StatefulWidget {
  final ArticlesRouteData data;

  const ArticlesScreen({
    Key? key,
    required this.data,
  });

  @override
  State<ArticlesScreen> createState() => _ArticlesScreenState();
}


class _ArticlesScreenState extends State<ArticlesScreen> {
  @override
  Widget build(BuildContext context) {
    String? image;
    String? title;
    String? description;
    UserData? user;

    if (widget.data.id == 0) {
      image = "assets/images/bloomberg.webp";
      title = "Bloomberg: Apple үйге арналған роботтар жасауды бастауы мүмкін";
      description = 'Apple компаниясының келесі үлкен жобасы тұтынушыларға күнделікті тапсырмаларды орындауға көмектесетін роботтарды жасау болуы мүмкін';
      user = UserData(
        id: 0, 
        username: "@kamalovbakhtiyor", 
        password: "Ba81906377", 
        first_name: "Бахтиёр", 
        last_name: "Камалов",
      );
    } else if (widget.data.id == 2) {
      image = "assets/images/it-technologies.jfif";
      title = "IT дегеніміз не және ол неге маңызды?";
      description = 'Сонымен, IT секторы дегеніміз не? Көшеде өтіп бара жатқан қарапайым адамнан сұрасаңыз, бірден ойға «бұл компьютерге қатысы бар нәрсе» және IT мамандары компьютермен жұмыс істейтін адамдар, сіз бірден «бағдарламашыларды» естисіз';
      user = UserData(
        id: 0, 
        username: "@kamalovbakhtiyor", 
        password: "Ba81906377", 
        first_name: "Бахтиёр", 
        last_name: "Камалов",
      );
    }

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pushNamed(context, "/"),
        ),
        title: Text(
          "Мақалалар", 
          style: TextStyle(
            fontSize: 18, 
            color: Color.fromARGB(255, 29, 29, 31),
          ),
        ),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 8.0),
            child: IconButton(
              onPressed: () {}, 
              icon: Icon(Icons.share_outlined, color: Color.fromARGB(255, 29, 29, 31)),
            ),
          ),
        ],
      ),
      body: ArticlesBody(data: ArticlesObjectData(
        image: image,
        title: title,
        description: description,
        user: UserData(
          id: user?.id,
          username: user?.username,
          password: user?.password,
          first_name: user?.first_name,
          last_name: user?.last_name, 
        ),
      )),
    );
  }
}
