import "package:flutter/material.dart";
import "package:thisis_kazakhstan/features/articles/widgets/all_articles_card.dart";
import "package:thisis_kazakhstan/features/user_data.dart";


class AllArticlesBody extends StatefulWidget {
  const AllArticlesBody({
    Key? key,
  });

  State<AllArticlesBody> createState() => _AllArticlesBodyState();
}


class _AllArticlesBodyState extends State<AllArticlesBody> {
  List<Map<String, dynamic>> articles = [
    {
      "id": 0,
      "user": UserData(
        id: 0,
        username: "@kamalovbakhtiyor",
        password: "Ba81906377",
        first_name: "Бахтиёр",
        last_name: "Камалов",
      ),
      "image": "assets/images/bloomberg.webp",
      "title": "Bloomberg: Apple үйге арналған роботтар жасауды бастауы мүмкін",
      "description": 'Apple компаниясының келесі үлкен жобасы тұтынушыларға күнделікті тапсырмаларды орындауға көмектесетін роботтарды жасау болуы мүмкін',
    },
    {
      "id": 2,
      "user": UserData(
        id: 0,
        username: "@kamalovbakhtiyor",
        password: "Ba81906377",
        first_name: "Бахтиёр",
        last_name: "Камалов",
      ),
      "image": "assets/images/it-technologies.jfif",
      "title": "IT дегеніміз не және ол неге маңызды?",
      "description": 'Сонымен, IT секторы дегеніміз не? Көшеде өтіп бара жатқан қарапайым адамнан сұрасаңыз, бірден ойға «бұл компьютерге қатысы бар нәрсе» және IT мамандары компьютермен жұмыс істейтін адамдар, сіз бірден «бағдарламашыларды» естисіз',
    },
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              Color.fromARGB(255, 201, 202, 205),
              Color.fromARGB(255, 222, 222, 222),
            ],
          ),
        ),
        padding: EdgeInsets.all(16.0),
        child: ListView(
          children: [
            for (int index = 0; index < articles.length; index++) Column(
              children: [
                AllArticlesCard(
                  id: articles[index]["id"],
                  user: articles[index]["user"],
                  image: articles[index]["image"],
                  title: articles[index]["title"],
                  description: articles[index]["description"],
                ),
                SizedBox(height: 16.0),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
