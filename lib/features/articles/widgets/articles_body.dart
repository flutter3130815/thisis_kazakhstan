import "package:flutter/material.dart";
import "package:thisis_kazakhstan/features/articles/view/articles_object_data.dart";
import "package:thisis_kazakhstan/features/articles/widgets/articles_header.dart";
import "package:thisis_kazakhstan/features/user_data.dart";


class ArticlesBody extends StatefulWidget {
  final ArticlesObjectData data;

  const ArticlesBody({
    Key? key,
    required this.data,
  });

  @override
  State<ArticlesBody> createState() => _NewsBodyState();
}


class _NewsBodyState extends State<ArticlesBody> {
  @override
  Widget build(BuildContext context) {
    String? image = widget.data.image;
    String? title = widget.data.title;
    String? description = widget.data.description;
    UserData? user = widget.data.user;

    return SafeArea(
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              Color.fromARGB(255, 201, 202, 205),
              Color.fromARGB(255, 222, 222, 222),
            ],
          ),
        ),
        child: ListView(
          children: [
            if (image != null && image.isNotEmpty) Container(
              height: 200,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(image),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(16.0),
              child: ArticlesHeader(
                title: title,
                description: description,
                user: user,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
