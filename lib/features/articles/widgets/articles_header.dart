import "package:flutter/material.dart";
import "package:thisis_kazakhstan/features/user_data.dart";
import "package:thisis_kazakhstan/features/widgets/base_widget.dart";


class ArticlesHeader extends StatelessWidget {
  final String? title;
  final String? description;
  final UserData? user;

  const ArticlesHeader({
    Key? key,
    required this.title,
    required this.description,
    required this.user,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          title != null && description != null ? Column(
            children: [
              Row(
                children: [
                  InkWell(
                    onTap: () {},
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: BaseWidget(
                        widget: Row(
                          children: [
                            Icon(
                              Icons.account_circle,
                              color: Color.fromARGB(255, 29, 29, 31),
                            ),
                            SizedBox(width: 6.0),
                            Text(
                              "${user?.username}",
                              style: TextStyle(
                                color: Color.fromARGB(255, 29, 29, 31),
                                fontSize: 16.0,
                              ),
                            ),
                            SizedBox(width: 6.0),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 16.0),
              BaseWidget(
                widget: Row(
                  children: [
                    Expanded(
                      child: Text(
                        title!,
                        style: TextStyle(
                          color: Color.fromARGB(255, 29, 29, 31),
                          fontSize: 20.0,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 16.0),
              BaseWidget(
                widget: Row(
                  children: [
                    Expanded(
                      child: Text(
                        description!,
                        style: TextStyle(
                          color: Color.fromARGB(255, 29, 29, 31),
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 16.0),
              BaseWidget(
                widget: Center(
                  child: Text(
                    "Түсіндірмесіз",
                    style: TextStyle(
                      color: Color.fromARGB(255, 29, 29, 31),
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
            ],
          ) : Center(
            child: Text("404 қатесы"),
          ),
        ],
      ),
    );
  }
}
