import "package:flutter/material.dart";
import "package:thisis_kazakhstan/features/home/widgets/home_body.dart";
import "package:thisis_kazakhstan/features/home/widgets/home_bottom_sheet.dart";


class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}


class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: HomeBody(),
      bottomSheet: HomeBottomSheet(),
    );
  }
}
