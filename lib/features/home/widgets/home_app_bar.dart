import "package:flutter/material.dart";
import "package:thisis_kazakhstan/features/articles/view/all_articles_screen.dart";
import "package:thisis_kazakhstan/features/news/view/all_news_screen.dart";

class HomeAppBar extends StatelessWidget {
  HomeAppBar({Key? key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        PopupMenuButton(
          tooltip: "Меню",
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          offset: Offset(0, 72),
          elevation: 0,
          color: Colors.white,
          itemBuilder: (BuildContext context) {
            return [
              PopupMenuItem(
                value: "Профиль",
                child: const Text(
                  "Профиль",
                  style: TextStyle(
                    color: Color.fromARGB(255, 29, 29, 31),
                    fontSize: 16.0,
                  ),
                ),
              ),
              PopupMenuItem(
                onTap: () => Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (context) => AllNewsScreen(),
                  ),
                  (route) => true,
                ),
                value: "Жаңалықтар",
                child: const Text(
                  "Жаңалықтар",
                  style: TextStyle(
                    color: Color.fromARGB(255, 29, 29, 31),
                    fontSize: 16.0,
                  ),
                ),
              ),
              PopupMenuItem(
                onTap: () => Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (context) => AllArticlesScreen(),
                  ),
                  (route) => true,
                ),
                value: "Мақалалар",
                child: const Text(
                  "Мақалалар",
                  style: TextStyle(
                    color: Color.fromARGB(255, 29, 29, 31),
                    fontSize: 16.0,
                  ),
                ),
              ),
              PopupMenuItem(
                value: "Премиум",
                child: const Text(
                  "Премиум",
                  style: TextStyle(
                    color: Color.fromARGB(255, 29, 29, 31),
                    fontSize: 16.0,
                  ),
                ),
              ),
              PopupMenuItem(
                value: "Қолдау қызметі",
                child: const Text(
                  "Қолдау қызметі",
                  style: TextStyle(
                    color: Color.fromARGB(255, 29, 29, 31),
                    fontSize: 16.0,
                  ),
                ),
              ),
            ];
          },
          onSelected: (value) {
            // Пусто
          },
          child: ClipRRect(
            borderRadius: BorderRadius.circular(56.0),
            child: Container(
              width: 56,
              height: 56,
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Icon(
                Icons.menu_outlined,
                color: Color.fromARGB(255, 29, 29, 31),
              ),
            ),
          ),
        ),
        SizedBox(width: 16.0),
        Expanded(
          child: ClipRRect(
            borderRadius: BorderRadius.circular(4.0),
            child: Container(
              padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
              height: 56,
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 29, 100, 242),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image(
                    image: AssetImage("assets/images/logo.png"),
                    semanticLabel: "Logo",
                    height: 46,
                    width: 46,
                  ),
                  SizedBox(width: 6.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: 4.0),
                        child: Text("This is",
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: "Kanit",
                              fontSize: 12,
                              fontWeight: FontWeight.w500,
                            )),
                      ),
                      Text("KAZAKHSTAN",
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: "Lemon",
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                            fontStyle: FontStyle.italic,
                          )),
                    ],
                  ),
                ],
              ),
              // child: Center(
              //   child: Text(
              //     "QAZAQ",
              //     style: TextStyle(
              //       fontSize: 24,
              //       letterSpacing: 6,
              //       fontWeight: FontWeight.bold,
              //       color: Color.fromARGB(255, 255, 215, 0),
              //     ),
              //   ),
              // ),
            ),
          ),
        ),
        SizedBox(width: 16.0),
        ClipRRect(
          borderRadius: BorderRadius.circular(56.0),
          child: Container(
            width: 56,
            height: 56,
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: IconButton(
              onPressed: () {},
              icon: Icon(Icons.search_outlined,
                  color: Color.fromARGB(255, 29, 29, 31)),
            ),
          ),
        ),
      ],
    );
  }
}
