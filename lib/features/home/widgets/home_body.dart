import "package:flutter/material.dart";
import "package:thisis_kazakhstan/features/home/widgets/home_app_bar.dart";
import "package:thisis_kazakhstan/features/home/widgets/home_header.dart";

class HomeBody extends StatefulWidget {
  const HomeBody({Key? key});

  @override
  State<HomeBody> createState() => _HomeBodyState();
}

class _HomeBodyState extends State<HomeBody> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              Color.fromARGB(255, 201, 202, 205),
              Color.fromARGB(255, 222, 222, 222),
            ],
          ),
          image: DecorationImage(
            image: AssetImage("assets/background_images/main.jpg"),
            fit: BoxFit.fill,
            colorFilter: ColorFilter.mode(
              Colors.black.withOpacity(0.6),
              BlendMode.darken,
            ),
          ),
        ),
        padding:
            EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0, bottom: 172.0),
        child: ListView(
          children: [
            HomeAppBar(),
            HomeHeader(),
          ],
        ),
      ),
    );
  }
}
