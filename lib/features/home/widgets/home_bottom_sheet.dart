import "package:flutter/material.dart";
import "package:flutter/widgets.dart";
import "package:thisis_kazakhstan/features/widgets/bottom_sheet_button.dart";

class HomeBottomSheet extends StatelessWidget {
  const HomeBottomSheet({Key? key});

  @override
  Widget build(BuildContext context) {
    return BottomSheet(
      onClosing: () {},
      builder: (context) {
        return Container(
          height: 170.0,
          padding: EdgeInsets.all(16.0),
          child: Column(
            children: [
              Text(
                "Жылдам әрекеттер",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 24,
                ),
              ),
              SizedBox(height: 16.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: BottomSheetButton(
                      text: "Жаңалық қосу",
                      size: 12.0,
                      icon: Icon(Icons.newspaper_outlined, color: Colors.white),
                      pressedMethod: () {},
                      color: Color.fromARGB(255, 29, 100, 242),
                    ),
                  ),
                  SizedBox(width: 12.0),
                  Expanded(
                    child: BottomSheetButton(
                      text: "Мақала қосыңыз",
                      size: 12.0,
                      icon: Icon(Icons.article_outlined, color: Colors.white),
                      pressedMethod: () {},
                      color: Color.fromARGB(255, 29, 100, 242),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 16.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: BottomSheetButton(
                      text: "Тапсырма қосыңыз",
                      size: 14.0,
                      icon: Icon(Icons.task_outlined, color: Colors.white),
                      pressedMethod: () {},
                      color: Color.fromARGB(255, 29, 100, 242),
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
