import "package:flutter/material.dart";
import "package:thisis_kazakhstan/features/my_tasks/view/my_tasks_screen.dart";
import "package:thisis_kazakhstan/features/news/view/news_screen.dart";
import "package:thisis_kazakhstan/features/news/view/news_route_data.dart";
import "package:thisis_kazakhstan/features/articles/view/articles_screen.dart";
import "package:thisis_kazakhstan/features/articles/view/articles_route_data.dart";


class HomeCard extends StatelessWidget {
  final int? id;
  final String? image;
  final String? title;
  final String? description;
  final bool? task_complete;
  final String? task_title;
  final String type;
  final dynamic color;
  final Icon icon;

  const HomeCard({
    Key? key, 
    required this.id,
    required this.image,
    required this.title,
    required this.description,
    required this.task_complete,
    required this.task_title,
    required this.type,
    required this.color,
    required this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (type == "news") {
          Navigator.pushAndRemoveUntil(
            context, 
            MaterialPageRoute(
              builder: (context) => NewsScreen(data: NewsRouteData(id: id))
            ),
            (route) => true,
          );
        } else if (type == "articles") {
          Navigator.pushAndRemoveUntil(
            context, 
            MaterialPageRoute(
              builder: (context) => ArticlesScreen(data: ArticlesRouteData(id: id))
            ),
            (route) => true,
          );
        } else if (type == "my_tasks") {
          Navigator.pushAndRemoveUntil(
            context, 
            MaterialPageRoute(
              builder: (context) => MyTasksScreen()
            ),
            (route) => true,
          );
        } else null;
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: Container(
          height: 90,
          decoration: BoxDecoration(
            color: Colors.white,
            image: image != null ? DecorationImage(
              image: AssetImage(image!),
              fit: BoxFit.cover,
              colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.4), BlendMode.darken,
              ),
            ) : null,
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(
              width: 2, 
              style: BorderStyle.solid, 
              color: color,
            ),
          ),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(10.0),
                    ),
                    child: Container(
                      height: 34,
                      width: 34,
                      decoration: BoxDecoration(
                        color: color,
                      ),
                      child: icon,
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: Padding(
                        padding: EdgeInsets.only(left: 6, right: 6),
                        child: Text(
                          title != null 
                            ? "${title}" 
                            : (task_complete == true ? "Орындалды" : "Орындалмаған"),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: TextStyle(
                            color: image != null ? Colors.white : Color.fromARGB(255, 29, 29, 31),
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.all(6),
                child: Text(
                  description != null ? "${description}" : "${task_title}",
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                    color: image != null ? Colors.white : Color.fromARGB(255, 29, 29, 31),
                    fontSize: 14,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
