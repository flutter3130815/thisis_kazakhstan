import "package:flutter/material.dart";
import "package:thisis_kazakhstan/features/home/widgets/home_card.dart";

class HomeCards extends StatelessWidget {
  final String type;
  final String title;
  final dynamic color;
  final String list_type;
  final Icon icon;
  List<Map<String, dynamic>> list;

  HomeCards({
    Key? key,
    required this.type,
    required this.title,
    required this.color,
    required this.list,
    required this.list_type,
    required this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: TextStyle(
            fontSize: 14,
            color: Colors.white.withOpacity(0.6),
            fontWeight: FontWeight.w600,
          ),
        ),
        SizedBox(height: 8.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            for (int index = 0; index < list.length; index++)
              Expanded(
                child: Padding(
                  padding: index.isEven
                      ? EdgeInsets.only(right: 8.0)
                      : EdgeInsets.only(left: 8.0),
                  child: Column(
                    children: [
                      list_type == "news_and_articles"
                          ? HomeCard(
                              id: list[index]["id"],
                              image: list[index]["image"],
                              title: list[index]["title"],
                              description: list[index]["description"],
                              task_complete: null,
                              task_title: null,
                              type: type,
                              color: color,
                              icon: icon,
                            )
                          : HomeCard(
                              id: list[index]["id"],
                              image: null,
                              title: null,
                              description: null,
                              task_complete: list[index]["task_complete"],
                              task_title: list[index]["task_title"],
                              type: type,
                              color: color,
                              icon: icon,
                            )
                    ],
                  ),
                ),
              ),
          ],
        ),
      ],
    );
  }
}
