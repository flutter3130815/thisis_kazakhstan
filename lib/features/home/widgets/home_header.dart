import "package:flutter/material.dart";
import "package:thisis_kazakhstan/features/home/widgets/home_cards.dart";

class HomeHeader extends StatefulWidget {
  HomeHeader({Key? key});

  State<HomeHeader> createState() => _HomeHeaderState();
}

class _HomeHeaderState extends State<HomeHeader> {
  List<Map<String, dynamic>> news = [
    {
      "id": 0,
      "image": "assets/images/minob.webp",
      "title":
          "Қорғаныс министрлігі Қазақстан аумағындағы Татарстандағы нысандарды атқылау туралы ақпаратқа түсініктеме берді",
      "description":
          'ҚР Қорғаныс министрлігінің хабарлауынша, Татарстан Республикасының нысандарына шабуыл жасады делінген ұшқышсыз ұшақтар Қазақстан аумағынан ұшырылғаны туралы Telegram арналарында тараған ақпарат шындыққа жанаспайды',
    },
    {
      "id": 2,
      "image": "assets/images/tokaev.webp",
      "title":
          "Тоқаев Батыс Қазақстан облысына келіп, эвакуацияланған тұрғындармен кездеседі",
      "description":
          'Қасым-Жомарт Тоқаев жұмыс сапарымен су тасқынынан зардап шеккен Батыс Қазақстан облысына келді',
    },
  ];

  List<Map<String, dynamic>> articles = [
    {
      "id": 0,
      "image": "assets/images/bloomberg.webp",
      "title": "Bloomberg: Apple үйге арналған роботтар жасауды бастауы мүмкін",
      "description":
          'Apple компаниясының келесі үлкен жобасы тұтынушыларға күнделікті тапсырмаларды орындауға көмектесетін роботтарды жасау болуы мүмкін',
    },
    {
      "id": 2,
      "image": "assets/images/it-technologies.jfif",
      "title": "IT дегеніміз не және ол неге маңызды?",
      "description":
          'Сонымен, IT секторы дегеніміз не? Көшеде өтіп бара жатқан қарапайым адамнан сұрасаңыз, бірден ойға «бұл компьютерге қатысы бар нәрсе» және IT мамандары компьютермен жұмыс істейтін адамдар, сіз бірден «бағдарламашыларды» естисіз',
    },
  ];

  List<Map<String, dynamic>> my_tasks = [
    {
      "id": 0,
      "task_title": "Бизнес жоспар жазыңыз",
      "task_complete": false,
    },
    {
      "id": 2,
      "task_title": "Flutter қолданбасына сервер қосыңыз",
      "task_complete": false,
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(0, 16, 0, 16),
      child: Column(
        children: [
          HomeCards(
            type: "news",
            title: "Жаңалықтар",
            color: Color.fromARGB(255, 255, 215, 0),
            list: news,
            list_type: "news_and_articles",
            icon: Icon(
              Icons.newspaper_outlined,
              size: 16,
              color: Colors.white,
            ),
          ),
          SizedBox(height: 16.0),
          HomeCards(
            type: "articles",
            title: "Мақалалар",
            color: Color.fromARGB(255, 255, 215, 0),
            list: articles,
            list_type: "news_and_articles",
            icon: Icon(
              Icons.article_outlined,
              size: 16,
              color: Colors.white,
            ),
          ),
          SizedBox(height: 16.0),
          HomeCards(
            type: "my_tasks",
            title: "Менің тапсырмаларым",
            color: Color.fromARGB(255, 29, 100, 242),
            list: my_tasks,
            list_type: "my_tasks",
            icon: Icon(
              Icons.task_outlined,
              size: 16,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }
}
