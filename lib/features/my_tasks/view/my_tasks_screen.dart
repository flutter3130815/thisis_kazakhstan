import "package:flutter/material.dart";
import "package:thisis_kazakhstan/features/my_tasks/widgets/my_tasks_body.dart";


class MyTasksScreen extends StatefulWidget {
  const MyTasksScreen({
    Key? key,
  });

  @override
  State<MyTasksScreen> createState() => _MyTasksScreenState();
}


class _MyTasksScreenState extends State<MyTasksScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
        icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pushNamed(context, "/"),
        ),
        title: Text(
          "Менің тапсырмаларым", 
          style: TextStyle(
            fontSize: 18, 
            color: Color.fromARGB(255, 29, 29, 31),
          ),
        ),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 8.0),
            child: IconButton(
              onPressed: () {}, 
              icon: Icon(Icons.search_outlined, color: Color.fromARGB(255, 29, 29, 31)),
            ),
          ),
        ],
      ),
      body: MyTasksBody(),
    );
  }
}
