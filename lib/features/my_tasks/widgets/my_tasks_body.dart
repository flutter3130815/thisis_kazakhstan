import "package:flutter/material.dart";
import "package:thisis_kazakhstan/features/my_tasks/widgets/my_tasks_header.dart";
import "package:thisis_kazakhstan/features/my_tasks/widgets/my_tasks_list.dart";


class MyTasksBody extends StatefulWidget {
  const MyTasksBody({
    Key? key,
  });

  @override
  State<MyTasksBody> createState() => _MyTasksBodyState();
}


class _MyTasksBodyState extends State<MyTasksBody> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              Color.fromARGB(255, 201, 202, 205),
              Color.fromARGB(255, 222, 222, 222),
            ],
          ),
        ),
        padding: EdgeInsets.all(16.0),
        child: ListView(
          children: [
            MyTasksHeader(),
            SizedBox(height: 16.0),
            MyTasksList(),
          ],
        ),
      ),
    );
  }
}
