import "package:flutter/material.dart";
import "package:thisis_kazakhstan/features/widgets/bottom_sheet_button.dart";


class MyTasksHeader extends StatelessWidget {
  const MyTasksHeader({
    Key? key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: BottomSheetButton(
                text: "Тапсырма қосыңыз",
                size: 14.0,
                color: Color.fromARGB(255, 1, 178, 245),
                icon: Icon(Icons.add_outlined, color: Colors.white),
                pressedMethod: () {},
              ),
            ),
          ],
        ),
        SizedBox(height: 12.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: BottomSheetButton(
                text: "Тапсырманы жою",
                size: 14.0,
                icon: Icon(Icons.remove_outlined, color: Colors.white),
                pressedMethod: () {},
                color: Color.fromARGB(255, 239, 62, 68),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
