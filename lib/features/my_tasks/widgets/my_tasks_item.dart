import "package:flutter/material.dart";
import "package:thisis_kazakhstan/features/my_tasks/widgets/my_tasks_item_data.dart";


class MyTasksItem extends StatefulWidget {
  final MyTasksItemData item_data;

  const MyTasksItem({
    Key? key,
    required this.item_data,
  });

  @override
  State<MyTasksItem> createState() => _MyTasksItemState();
}


class _MyTasksItemState extends State<MyTasksItem> {
  @override
  Widget build(BuildContext context) {
    MyTasksItemData item_data = widget.item_data;

    void _toggle_task_complete(int index) {
      setState(() {
        item_data.task_complete = !item_data.task_complete;
      });

      // if (my_tasks[index]["task_complete"]) {
      //   changed_items.add(index);
      // } else {
      //   changed_items.remove(index);
      // }
    }

    return ClipRRect(
      borderRadius: BorderRadius.circular(10.0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: CheckboxListTile(
          title: Text(
            item_data.task_title,
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w500,
              color: Color.fromARGB(255, 29, 29, 31),
            ),
          ),
          secondary: Icon(
            Icons.task_outlined, 
            color: Color.fromARGB(255, 1, 178, 245),
          ),
          value: item_data.task_complete, 
          onChanged: (newValue) => _toggle_task_complete(0),
          activeColor: Color.fromARGB(255, 1, 178, 245),
          checkColor: Colors.white,
        ),
      ),
    );
  }
}
