class MyTasksItemData {
  bool task_complete;
  String task_title;

  MyTasksItemData({
    required this.task_complete,
    required this.task_title,
  });
}
