import "package:flutter/material.dart";
import "package:thisis_kazakhstan/features/my_tasks/widgets/my_tasks_item.dart";
import "package:thisis_kazakhstan/features/my_tasks/widgets/my_tasks_item_data.dart";


class MyTasksList extends StatefulWidget {
  const MyTasksList({
    Key? key,
  });

  @override
  State<MyTasksList> createState() => _MyTasksListState();
}


class _MyTasksListState extends State<MyTasksList> {
  @override
  Widget build(BuildContext context) {
    List<int> changed_items = [];
    List<Map<String, dynamic>> my_tasks = [
      {
        "id": 0,
        "task_title": "Бизнес жоспар жазыңыз",
        "task_complete": false,
      },
      {
        "id": 2,
        "task_title": "Flutter қолданбасына сервер қосыңыз",
        "task_complete": false,
      },
    ];

    return Column(
      children: [
        for (var index = 0; index < my_tasks.length; index++) Column(
          children: [
            MyTasksItem(
              item_data: MyTasksItemData(
                task_complete: my_tasks[index]["task_complete"],
                task_title: my_tasks[index]["task_title"],
              ),
            ),
            SizedBox(height: 12.0),
          ],
        ),
      ],
    );
  }
}
