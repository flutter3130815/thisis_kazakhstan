import "package:flutter/material.dart";
import "package:thisis_kazakhstan/features/news/widgets/all_news_body.dart";


class AllNewsScreen extends StatelessWidget {
  const AllNewsScreen({
    Key? key,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
        icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pushNamed(context, "/"),
        ),
        title: Text(
          "Жаңалықтар", 
          style: TextStyle(
            fontSize: 18, 
            color: Color.fromARGB(255, 29, 29, 31),
          ),
        ),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 8.0),
            child: IconButton(
              onPressed: () {}, 
              icon: Icon(Icons.share_outlined, color: Color.fromARGB(255, 29, 29, 31)),
            ),
          ),
        ],
      ),
      body: AllNewsBody(),
    );
  }
}
