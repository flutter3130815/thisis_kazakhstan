import 'package:thisis_kazakhstan/features/user_data.dart';


class NewsObjectData {
  String? image;
  String? title;
  String? description;
  UserData? user;

  NewsObjectData({
    required this.image,
    required this.title,
    required this.description,
    required this.user,
  });
}
