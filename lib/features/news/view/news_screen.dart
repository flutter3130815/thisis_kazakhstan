import "package:flutter/material.dart";
import "package:thisis_kazakhstan/features/news/view/news_object_data.dart";
import "package:thisis_kazakhstan/features/news/view/news_route_data.dart";
import "package:thisis_kazakhstan/features/news/widgets/news_body.dart";
import "package:thisis_kazakhstan/features/user_data.dart";


class NewsScreen extends StatefulWidget {
  final NewsRouteData data;

  NewsScreen({
    Key? key,
    required this.data,
  });

  @override
  State<NewsScreen> createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen> {
  @override
  Widget build(BuildContext context) {
    String? image;
    String? title;
    String? description;
    UserData? user;

    if (widget.data.id == 0) {
      image = "assets/images/minob.webp";
      title = "Қорғаныс министрлігі Қазақстан аумағындағы Татарстандағы нысандарды атқылау туралы ақпаратқа түсініктеме берді";
      description = "ҚР Қорғаныс министрлігінің хабарлауынша, Татарстан Республикасының нысандарына шабуыл жасады делінген ұшқышсыз ұшақтар Қазақстан аумағынан ұшырылғаны туралы Telegram арналарында тараған ақпарат шындыққа жанаспайды";
      user = UserData(
        id: 0, 
        username: "@kamalovbakhtiyor", 
        password: "Ba81906377", 
        first_name: "Бахтиёр", 
        last_name: "Камалов",
      );
    } else if (widget.data.id == 2) {
      image = "assets/images/tokaev.webp";
      title = "Тоқаев Батыс Қазақстан облысына келіп, эвакуацияланған тұрғындармен кездеседі";
      description = "Қасым-Жомарт Тоқаев жұмыс сапарымен су тасқынынан зардап шеккен Батыс Қазақстан облысына келді";
      user = UserData(
        id: 0, 
        username: "@kamalovbakhtiyor", 
        password: "Ba81906377", 
        first_name: "Бахтиёр", 
        last_name: "Камалов",
      );
    }

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
        icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pushNamed(context, "/"),
        ),
        title: Text(
          "Жаңалықтар", 
          style: TextStyle(
            fontSize: 18, 
            color: Color.fromARGB(255, 29, 29, 31),
          ),
        ),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 8.0),
            child: IconButton(
              onPressed: () {}, 
              icon: Icon(Icons.share_outlined, color: Color.fromARGB(255, 29, 29, 31)),
            ),
          ),
        ],
      ),
      body: NewsBody(data: NewsObjectData(
        image: image,
        title: title,
        description: description,
        user: UserData(
          id: user?.id,
          username: user?.username,
          password: user?.password,
          first_name: user?.first_name,
          last_name: user?.last_name, 
        ),
      )),
    );
  }
}
