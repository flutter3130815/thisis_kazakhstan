import "package:flutter/material.dart";
import "package:thisis_kazakhstan/features/news/widgets/all_news_card.dart";
import "package:thisis_kazakhstan/features/user_data.dart";


class AllNewsBody extends StatefulWidget {
  const AllNewsBody({
    Key? key,
  });

  State<AllNewsBody> createState() => _AllNewsBodyState();
}


class _AllNewsBodyState extends State<AllNewsBody> {
  List<Map<String, dynamic>> news = [
    {
      "id": 0,
      "user": UserData(
        id: 0,
        username: "@kamalovbakhtiyor",
        password: "Ba81906377",
        first_name: "Бахтиёр",
        last_name: "Камалов",
      ),
      "image": "assets/images/minob.webp",
      "title": "Қорғаныс министрлігі Қазақстан аумағындағы Татарстандағы нысандарды атқылау туралы ақпаратқа түсініктеме берді",
      "description": 'ҚР Қорғаныс министрлігінің хабарлауынша, Татарстан Республикасының нысандарына шабуыл жасады делінген ұшқышсыз ұшақтар Қазақстан аумағынан ұшырылғаны туралы Telegram арналарында тараған ақпарат шындыққа жанаспайды',
    },
    {
      "id": 2,
      "user": UserData(
        id: 0,
        username: "@kamalovbakhtiyor",
        password: "Ba81906377",
        first_name: "Бахтиёр",
        last_name: "Камалов",
      ),
      "image": "assets/images/tokaev.webp",
      "title": "Тоқаев Батыс Қазақстан облысына келіп, эвакуацияланған тұрғындармен кездеседі",
      "description": 'Қасым-Жомарт Тоқаев жұмыс сапарымен су тасқынынан зардап шеккен Батыс Қазақстан облысына келді',
    },
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              Color.fromARGB(255, 201, 202, 205),
              Color.fromARGB(255, 222, 222, 222),
            ],
          ),
        ),
        padding: EdgeInsets.all(16.0),
        child: ListView(
          children: [
            for (int index = 0; index < news.length; index++) Column(
              children: [
                AllNewsCard(
                  id: news[index]["id"],
                  user: news[index]["user"],
                  image: news[index]["image"],
                  title: news[index]["title"],
                  description: news[index]["description"],
                ),
                SizedBox(height: 16.0),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
