import "package:flutter/material.dart";
import "package:flutter/widgets.dart";
import "package:thisis_kazakhstan/features/news/view/news_route_data.dart";
import "package:thisis_kazakhstan/features/news/view/news_screen.dart";
import "package:thisis_kazakhstan/features/user_data.dart";

class AllNewsCard extends StatelessWidget {
  final int id;
  final UserData user;
  final String image;
  final String title;
  final String description;

  const AllNewsCard({
    Key? key,
    required this.id,
    required this.user,
    required this.image,
    required this.title,
    required this.description,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
            builder: (context) => NewsScreen(data: NewsRouteData(id: id))),
        (route) => true,
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Column(
            children: [
              Container(
                height: 120,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(image),
                    fit: BoxFit.cover,
                    colorFilter: ColorFilter.mode(
                      Colors.black.withOpacity(0.4),
                      BlendMode.darken,
                    ),
                  ),
                ),
                padding: EdgeInsets.all(10.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Expanded(
                      child: Text(
                        title,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          child: Row(
                            children: [
                              Icon(
                                Icons.account_circle,
                                color: Color.fromARGB(255, 29, 29, 31),
                              ),
                              SizedBox(width: 4.0),
                              Text(
                                "${user.username}",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 29, 29, 31),
                                  fontSize: 14,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 10.0),
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            description,
                            style: TextStyle(
                              color: Color.fromARGB(255, 29, 29, 31),
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
