import "package:flutter/material.dart";
import "package:thisis_kazakhstan/features/news/view/news_object_data.dart";
import "package:thisis_kazakhstan/features/news/widgets/news_header.dart";
import "package:thisis_kazakhstan/features/user_data.dart";


class NewsBody extends StatefulWidget {
  final NewsObjectData data;

  const NewsBody({
    Key? key,
    required this.data,
  });

  @override
  State<NewsBody> createState() => _NewsBodyState();
}


class _NewsBodyState extends State<NewsBody> {
  @override
  Widget build(BuildContext context) {
    String? image = widget.data.image;
    String? title = widget.data.title;
    String? description = widget.data.description;
    UserData? user = widget.data.user;

    return SafeArea(
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              Color.fromARGB(255, 201, 202, 205),
              Color.fromARGB(255, 222, 222, 222),
            ],
          ),
        ),
        child: ListView(
          children: [
            if (image != null && image.isNotEmpty) Container(
              height: 200,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(image),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(16.0),
              child: NewsHeader(
                title: title,
                description: description,
                user: user,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
