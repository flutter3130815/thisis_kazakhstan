class UserData {
  final int? id;
  String? username;
  String? password;
  String? first_name;
  String? last_name;

  UserData({
    required this.id,
    required this.username,
    required this.password,
    required this.first_name,
    required this.last_name,
  });
}
