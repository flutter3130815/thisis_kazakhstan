import "package:flutter/material.dart";


class BaseWidget extends StatelessWidget {
  dynamic widget;

  BaseWidget({
    Key? key,
    required this.widget,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Colors.white,
      ),
      padding: EdgeInsets.all(10.0),
      child: widget,
    );
  }
}
