import "package:flutter/material.dart";


class BottomSheetButton extends StatelessWidget {
  final String text;
  final double size;
  final Function pressedMethod;
  final Icon icon;
  final dynamic color;

  const BottomSheetButton({
    Key? key, 
    required this.text, 
    required this.size,
    required this.icon, 
    required this.pressedMethod,
    required this.color,
  });

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
      child: Container(
        height: 36,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          border: Border.all(
            width: 2, 
            style: BorderStyle.solid, 
            color: color,
          ),
        ),
        child: Row(
          children: [
            Container(
              height: 36,
              width: 36,
              color: color,
              child: icon,
            ),
            Expanded(
              child: TextButton(
                onPressed: this.pressedMethod(),
                child: Text(
                  this.text, 
                  style: TextStyle(
                    fontSize: size,
                    color: Color.fromARGB(255, 29, 29, 31),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
