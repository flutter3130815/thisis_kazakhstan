import "package:thisis_kazakhstan/features/articles/view/all_articles_screen.dart";
import "package:thisis_kazakhstan/features/articles/view/articles_route_data.dart";
import "package:thisis_kazakhstan/features/articles/view/articles_screen.dart";
import "package:thisis_kazakhstan/features/home/view/home_screen.dart";
import "package:thisis_kazakhstan/features/my_tasks/view/my_tasks_screen.dart";
import "package:thisis_kazakhstan/features/news/view/all_news_screen.dart";
import "package:thisis_kazakhstan/features/news/view/news_route_data.dart";
import "package:thisis_kazakhstan/features/news/view/news_screen.dart";


final routes = {
  "/" : (context) => HomeScreen(),
  "/news": (context) => NewsScreen(data: NewsRouteData(id: null)),
  "/articles": (context) => ArticlesScreen(data: ArticlesRouteData(id: null)),
  "/my_tasks": (context) => MyTasksScreen(),
  "/all_news": (context) => AllNewsScreen(),
  "/all_articles": (context) => AllArticlesScreen(),
};
