import "dart:ui";

import "package:flutter/material.dart";


final lightTheme = ThemeData(
  fontFamily: "Montserrat",
  buttonTheme: ButtonThemeData(
    buttonColor: Color.fromARGB(255, 29, 29, 31),
    textTheme: ButtonTextTheme.normal,
  ),
  textTheme: TextTheme(
    bodyMedium: TextStyle(
      color: Color.fromARGB(255, 29, 29, 31),
      fontSize: 18,
    ),
    labelSmall: TextStyle(
      color: Colors.black.withOpacity(0.6),
      fontWeight: FontWeight.w600,
      fontSize: 14,
    ),
  ),
);

final darkTheme = ThemeData(
  scaffoldBackgroundColor: const Color.fromARGB(255, 15, 15, 15),
  fontFamily: "Montserrat",
  buttonTheme: ButtonThemeData(
    buttonColor: Colors.white,
    textTheme: ButtonTextTheme.normal,
  ),
  textTheme: TextTheme(
    bodyMedium: TextStyle(
      color: Colors.white,
      fontSize: 18,
    ),
    labelSmall: TextStyle(
      color: Colors.white.withOpacity(0.6),
      fontWeight: FontWeight.w600,
      fontSize: 14,
    ),
  ),
);
