import "package:flutter/material.dart";
import "package:thisis_kazakhstan/router/router.dart";
import "package:thisis_kazakhstan/theme/theme.dart";
import "package:thisis_kazakhstan/features/home/home.dart";


class ThisisKazakhstanApp extends StatelessWidget {
  const ThisisKazakhstanApp({Key? key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "This is Kazakhstan",
      theme: lightTheme,
      routes: routes,
    );
  }
}
